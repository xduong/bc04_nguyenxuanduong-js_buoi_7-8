// console.log("duong handsome");

function getId(ele) {
  return document.getElementById(ele);
}

var numArr = [];

var positiveNumArr = []; // bài 1.2.4.4

var minPositive = positiveNumArr[0]; // bài 4

getId("addNum").onclick = function () {
  // alert(2);
  // hàm này để tạo thành các mảng
  var num = getId("num").value * 1;
  getId("num").value = "";
  numArr.push(num);
  //xuất ra mảng số
  getId("printNumArr").innerHTML = numArr;

  // mảng chứa các số dương
  // có 2 cách để tạo ra mảng số dương
  // cách 1: filter từ mảng thô
  // cách 2: lúc add số vào mình xét nó có dương hay không thì push vào mảng số dương luôn
  // về mặt tốc độ xử lý thì chắc cách 2 sẽ cho performance cao hơn
  // do đang học vể method nên mình vẫn sẽ dùng cách 1

  // mảng chứa các số dương
  positiveNumArr = numArr.filter(function (number) {
    return number > 0;
  });

  // mảng chứa các số âm
  negativeNumArr = numArr.filter(function (number) {
    return number < 0;
  });

  //mảng chứa các số chẵn
  evenNumArr = numArr.filter(function (number) {
    if (number % 2 == 0) return number;
  });

  // mảng số nguyên
  integerNumArr = numArr.filter(function (number) {
    if (Number.isInteger(number) == true) return number;
  });
  console.log("integerNumArr: ", integerNumArr);
};

/* Bài 1 */
// có 3 cách tính:
// cách 1 là dùng forEach và if cho mảng ban đầu
// cách 2 là dùng forEach cho mảng số dương
// cách 3 dùng reduce cho mảng số dương

var sumPositive = 0;

getId("tinhTong_bai1").onclick = function () {
  positiveNumArr.forEach(function (number) {
    sumPositive += number;
  });
  getId("ketQua_bai1").innerHTML =
    "Tổng các số dương của dãy là: " + sumPositive;
};

/* Bài 2 */
var countPositive = 0;

getId("countSoDuong_bai2").onclick = function () {
  countPositive = positiveNumArr.length;
  getId("ketQua_bai2").innerHTML = countPositive;
};

/* Bài 3*/

getId("timMin_bai3").onclick = function () {
  var min = positiveNumArr[0];

  numArr.forEach(function (number) {
    if (number <= min) {
      min = number;
    }
  });
  getId("ketQua_bai3").innerHTML = "Số nhỏ nhất là: " + min;
};

/* Bài 4*/

getId("btn_bai4").onclick = function () {
  var minPositive = positiveNumArr[0];

  positiveNumArr.forEach(function (number) {
    if (number <= minPositive) {
      minPositive = number;
    }
  });
  getId("ketQua_bai4").innerHTML = "Số nhỏ nhất là: " + minPositive;
};

/* Bài 5*/

getId("btn_bai5").onclick = function () {
  var endEvenNum = evenNumArr[evenNumArr.length - 1];
  getId("ketQua_bai5").innerHTML = "Số chẵn cuối cùng: " + endEvenNum;
};
/* Bài 6*/
getId("btn_bai6").onclick = function () {
  var changeNumArr = numArr;

  var viTri1 = getId("viTri1_bai6").value * 1;
  var viTri2 = getId("viTri2_bai6").value * 1;

  var valueViTri1 = numArr[viTri1];
  var valueViTri2 = numArr[viTri2];

  var blank = valueViTri1;

  changeNumArr[viTri1] = valueViTri2;
  changeNumArr[viTri2] = blank;

  getId("ketQua_bai6").innerHTML = "Mảng sau khi đổi là: " + changeNumArr;
  console.log("numArr: ", numArr);
};

/* Bài 7*/

getId("btn_bai7").onclick = function () {
  var temp = "";
  for (var index = 0; index < numArr.length; index++) {
    for (var j = 0; j < numArr.length; j++) {
      if (numArr[index] < numArr[j]) {
        temp = numArr[j];
        numArr[j] = numArr[index];
        numArr[index] = temp;
      }
    }
  }

  getId("ketQua_bai7").innerHTML = numArr;
};

/* Bài 8*/

// hàm kiểm tra só nguyên tố nếu là số nguyên tố hàm trả về 1 nếu không thì trả về 0

kiemTraSoNguyenTo = function (x) {
  if (x < 2) {
    return 0;
  } else if (x == 2 || x == 3) {
    return 1;
  } else if (x % 2 == 0) {
    return 0;
  } else {
    for (var i = 3; i < x; i += 2) {
      if (x % i == 0) {
        return 0;
      } else return 1;
    }
  }
};
// console.log("kiemTraSoNguyenTo: ", kiemTraSoNguyenTo(3));

getId("btn_bai8").onclick = function () {
  for (var index = 0; index < numArr.length; index++) {
    if (kiemTraSoNguyenTo(numArr[index])) break;
  }
  getId("ketQua_ba8i").innerHTML = numArr[index];
};

/* Bài 9*/
getId("btn_bai9").onclick = function () {
  getId("ketQua_bai9").innerHTML = integerNumArr.length;
};
/* Bài 10*/
getId("btn_bai10").onclick = function () {
  var result_10 = "";
  if (positiveNumArr.length > negativeNumArr.length) {
    result_10 = "Số dương > Số âm";
  } else if (positiveNumArr.length < negativeNumArr.length) {
    result_10 = "Số dương < Số âm";
  } else result_10 = "Số dương = Số âm";
  getId("ketQua_bai10").innerHTML = result_10;
};
